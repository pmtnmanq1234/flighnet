import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Created by Rob Thomas on Dec-2016.
 */
public class RawList {
    private List<String> rawLst = new ArrayList<>();
    private List<String> rawLstSortable = new ArrayList<>();
    public RawList() {
        // create the list
        // assume objects exist and content vetted.
        crtRawLst("Provider1.txt");
        crtRawLst("Provider2.txt");
        crtRawLst("Provider3.txt");
        crtRawLst("Provider4.txt");

        // Remove the duplicates
        Set<String> hs = new HashSet<>();
        hs.addAll(rawLst);
        rawLst.clear();
        rawLst.addAll(hs);

        // create sortable raw list as per requirement
        // 3. Order by two fields: by “Price” first in descending order
        // (lowest price first) and by “Departure Time” next (earlier flight first).
        // format template as per the following:
        // orgDst    priceTime     raw index
        // xxxxxx~99999.9999:99:99~9
        for (int i = 0; i < rawLst.size(); i++) {
            // get the orgDst from list
            String[] spl = rawLst.get(i).split(",");
            String orgDst = spl[0] + spl[2];

            // get depart time from list
            String[] splDepart = spl[1].split("\\s");
            // pad hour with zero to normailze sort value
            String[] splTime = splDepart[1].split(":");
            int intSpl = Integer.parseInt(splTime[0]);
            String departTime = String.format("%02d",intSpl) + ":" + splTime[1] + ":" +splTime[2];

            //get the price from list
            spl = rawLst.get(i).split("\\$");
            // pad price with zeros to normailze sort value
            // 99999.99 assuming price per flight less then $100,000
            String[] splPrice = spl[1].split("\\.");
            intSpl = Integer.parseInt(splPrice[0]);
            String price = String.format("%05d",intSpl) + "." + splPrice[1];

            // create normalized and sorted reference flight array object
            rawLstSortable.add(orgDst + "~" + price + departTime + "~" + i);
        }

        // sort the list
        Collections.sort(rawLstSortable);
    }

    // create one arrayList file from three txt files
    private void crtRawLst (String txtFile){
        Stream<String> stream = null;
        try {
            stream = Files.lines(Paths.get(txtFile));
        } catch (IOException e) {
            e.getMessage();
            e.printStackTrace();
        }
        // assume provider files not empty and vetted
        List<String> lst = stream != null ? stream.collect(Collectors.toList()) : null;
        for (String s : lst) {
            //  normalize text file disparities
            String tmpStr = s.replace('|', ',');
            tmpStr = tmpStr.replace('-','/');
            rawLst.add(tmpStr);
        }
    }
    // get the idx reference value fromthe raw flight data list.
    // return list of array references
    public List<String> getFlightIdx(String flight){
        List<String> flighIdx = new ArrayList<>();
        rawLstSortable.forEach(elm->{
            String[] spl = elm.split("~");
            if (spl[0].equals(flight)) flighIdx.add(spl[2]);
        });
        return flighIdx;
    }
    // return the formatted flight data as per requirement:
    // {Origin} --> {Destination} ({Departure Time} --> {Destination Time}) - {Price}
    // MIA,6/20/2014 7:45:00,ORD,6/20/2014 12:36:00,$422.00
    public String getFlightData(String idx){
        int intSpl = Integer.parseInt(idx);

        // MIA,6/20/2014 7:45:00,ORD,6/20/2014 12:36:00,$422.00
        // [0] [1]               [2] [3]                [4]
        String[] spl = rawLst.get(intSpl).split(",");

        return spl[0] + " --> " + spl[2] + " (" + spl[1] + " --> " + spl[3] + ") - " + spl[4];
    }
    // Testing
    public List<String> getRawLst(){
        return rawLst;
    }
    // Testing
    public List<String> getRawLstSortable(){
        return rawLstSortable;
    }
}
