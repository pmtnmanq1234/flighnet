import java.util.List;

/**
 * Created by Rob Thomas on Dec-2016.
 */
public class SearchFlights  {

    public static void main(String[] args) {
        // validate command line args
        new CmdArgs().parse(args);
        // get command line arg values
        String ordDst = CmdArgs.getArg("o")+ CmdArgs.getArg("d");

        // consolidate and sort flight files
        RawList lst = new RawList();

        // testing
        // lst.getRawLst().forEach(System.out::println);
        // lst.getRawLstSortable().forEach(System.out::println);

        // format and print flights
        List<String> flightIdx = lst.getFlightIdx(ordDst);
        if (!flightIdx.isEmpty()) {
            for (String s : flightIdx) {
                System.out.println(lst.getFlightData(s));
            }
        } else {
            System.out.println("No Flights Found for "
                    + CmdArgs.getArg("o") + " --> " + CmdArgs.getArg("d"));
        }
    }
}
