
import org.apache.commons.cli.*;


/**
 * Created by Rob Thomas on Dec-2016.
 */
public class CmdArgs {
    public Options options = new Options();
    private static CommandLine cmd = null;
    public CmdArgs() {
        options.addOption("h","help",false,"Usage: -o YYZ -d YYC");
        options.addOption("o","org",true,"Enter orgination airport code. eg. YYZ");
        options.addOption("d","dst",true,"Enter destination airport code. eg. YYC");
    }
    public void parse(String [] args) {
        CommandLineParser parser = new DefaultParser();
        try {
            cmd = parser.parse(options,args);

            // help option selected
            if (cmd.hasOption("h")) {
                help();
            }
            if (cmd.getOptionValue("o") == null || cmd.getOptionValue("d")== null) {
                help();
            }
        } catch (ParseException e) {
            System.out.println("Failed to parse command line: " + e.getMessage());
            help();
        }
    }
    public static String getArg (String arg){
        return cmd.getOptionValue(arg).toUpperCase();
    }
    private void help () {
        HelpFormatter formater = new HelpFormatter();
        formater.printHelp("CmdArgs", options);
        System.exit(0);
    }
}